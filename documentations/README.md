# Proxy Logger

## Introduction

Sometime you need to ensure to have a logger even without a target at the end to 
ensure you code will always work and have the logger set.

This require to be able to dynamically change the logger in object already created, which
complicate the development.

A proxy allow you to have a defined logger and be able to change the target logger in
a central place

## Services

See [services](services/README.md)

## Loggers

### Logger

[Root interface](../src/lib/logger.ts)

### StandardLogger

[StandardLogger](../src/lib/logger.ts) add default log level method

### GenericLogger

[GenericLogger](../src/lib/logger.ts) allow a logegr to have a parent and a target

### Target Logger

[TargetLogger](../src/lib/target-logger.ts) is used to send the user log.

### ProxyLogger

Extends `GenericLogger`

You may find a base class for proxy logger in [BaseProxyLogger](base-proxy-logger.md).

### Built-in proxy

The module provide some predefined proxy for some logger.
- [SimpleGenericProxyLogger](simple-generic-proxy-logger.md)

## Debug

The module use internally [debug](https://www.npmjs.com/package/debug) with the root prefix
`ze:proxy-logger`.

## Usage

See [USAGE.md](USAGE.md)
