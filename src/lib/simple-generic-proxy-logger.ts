import {BaseProxyLogger} from "./base-proxy-logger";

/**
 * A simple generic proxy logger
 */
export class SimpleGenericProxyLogger extends BaseProxyLogger {

}
