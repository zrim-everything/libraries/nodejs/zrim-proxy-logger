import {ProxyLogger} from "./proxy-logger";
import {TargetLogger} from "./target-logger";

export interface LoggerManagerHandler {
  /**
   * Get a logger from the name
   * @param name The logger name
   * @throws {Error} If the logger cannot be created
   */
  getLogger(name?: string): ProxyLogger;

  /**
   * List the logger names available
   * @throws {Error} If the list cannot be retrieve
   */
  listLoggerNames(): string[];

  /**
   * Get the target
   * @param name The target name
   */
  getTarget(name?: string): TargetLogger | undefined;

  /**
   * Modify the logger target.
   *
   * The effect may depend on the manager implementation
   * @param name The target name
   * @param target The target to set
   * @throws {Error} If the logger cannot be set
   */
  setTarget(name?: string, target?: TargetLogger): void;

  /**
   * List the target names available
   * @throws {Error} If the list cannot be retrieve
   */
  listTargetNames(): string[];
}
