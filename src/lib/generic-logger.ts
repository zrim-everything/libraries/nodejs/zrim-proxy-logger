import {StandardLogger} from "./logger";
import {TargetLogger} from "./target-logger";
import {ProxyLogger} from "./proxy-logger";

export namespace genericLogger {

  /**
   * Generate a meta data
   */
  export type LoggerMetaDataGenerator = (owner: ProxyLogger, options: SerializeMetaDataOptions) => object | null | undefined;

  /**
   * Proxy meta data
   */
  export type LoggerMetaData = LoggerMetaDataGenerator | string | number | boolean | object | undefined | null;

  export interface OfOptions {
    /**
     * The prefixes to add
     */
    prefixes?: string[] | undefined;

    /**
     * Default meta data to add
     */
    metaData?: LoggerMetaData;

    /**
     * A new target to use
     */
    target?: TargetLogger | undefined;
  }

  /**
   * Options to generate the meta data
   */
  export interface SerializeMetaDataOptions {
    /**
     * Possible user argument given during log call
     */
    userArguments?: any[] | undefined;
  }
}

/**
 * Generic logger
 */
export interface GenericLogger extends StandardLogger {

  /**
   * The parent logger
   */
  readonly parent?: GenericLogger | undefined;

  /**
   * The log target
   */
  target?: TargetLogger | undefined | null;

  /**
   * The meta data
   */
  metaData?: genericLogger.LoggerMetaData;

  /**
   * The logger prefixes
   */
  prefixes: string[];

  /**
   * Create a child logger
   * @param options
   */
  of(options?: genericLogger.OfOptions): GenericLogger;

  /**
   * Generate the meta data for the log
   * @param options
   */
  serializeMetaData(options: genericLogger.SerializeMetaDataOptions): object | undefined;
}
