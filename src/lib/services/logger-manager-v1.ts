import {LoggerManager} from "../logger-manager";
import {ProxyLogger} from "../proxy-logger";
import {TargetLogger} from "../target-logger";

/**
 * Logger manager specification v1
 */
export interface LoggerManagerV1 extends LoggerManager {

  /**
   * Returns the known logger names
   */
  listLoggerNames(): string[];

  /**
   * Returns the known target logger names
   */
  listTargetLoggerNames(): string[];

  /**
   * Get the logger with the given name.
   * The current implementation returns a proxy logger. This allow to change dynamically the target on the fly
   * @param loggerName The logger name
   */
  getLogger(loggerName?: string | null | undefined): ProxyLogger;

  /**
   * Returns the logger target with the given name
   * @param loggerName The logger name
   */
  getTargetLogger(loggerName: string | undefined | null): TargetLogger | undefined;

  /**
   * Modify the logger target. In case the target is nil, this equivalent of removing it
   * @param loggerName The logger name
   * @param target The target
   */
  setTargetLogger(loggerName: string | undefined | null, target?: TargetLogger): void;

  /**
   * Remove all the targets.
   * Be careful, this also remove the default one.
   * @see listLoggerNames
   * @see setLoggerTarget
   */
  removeAllTargets(): void;
}
